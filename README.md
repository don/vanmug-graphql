# GraphQL & Mongo

To install, clone this repository to your local machine.

`cd` into the directory and run `npm install`

Then you can run `npm start` to start up the server.

From there you can visit `http://localhost:4000/graphiql` to play in the playground or connect a tool such as [GraphiQL](https://electronjs.org/apps/graphiql) to `http://localhost:4000`.


