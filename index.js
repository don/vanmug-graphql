const MONGO_URL                 = 'mongodb://localhost:27017/blog'
const DB_NAME                   = "GraphQL_posts";

const assert                    = require('assert');
const { ApolloServer }          = require('apollo-server');
const { MongoClient, ObjectId } = require('mongodb');
const typeDefs                  = require('./typedefs');

const prepare = (o) => {
  if (o._id) {
    o._id = o._id.toString()
  }
  return o;
};

const client = new MongoClient(MONGO_URL, { useNewUrlParser: true });

client.connect(err => {
  assert.equal(null, err);

  const db = client.db(DB_NAME);
  const Posts = db.collection('posts');
  const Comments = db.collection('comments');


  // Resolvers define the technique for fetching the types defined in the schema.  

  const resolvers = {
    Query: {
      post: async (root, {id}) => {
        return prepare(await Posts.findOne({"_id": ObjectId(id)}));
      },
      posts: async () => {
        return (await Posts.find({}).toArray()).map(prepare);
      },
      comment: async (root, {id}) => {
        return prepare(await Comments.findOne(ObjectId(id)));
      },
    },
    Mutation: {
      createPost: async (root, args, context, info) => {
        const res = await Posts.insertOne(args);
        return prepare(await Posts.findOne({_id: res.insertedId}));
      },
      createComment: async (root, args) => {
        const res = await Comments.insertOne(args);
        return prepare(await Comments.findOne({_id: res.insertedId}));
      },
    },
    Post: {
      comments: async ({_id}) => {
        return (await Comments.find({postId: _id}).toArray()).map(prepare);
      }
    },
    Comment: {
      post: async ({postId}) => {
        return prepare(await Posts.findOne(ObjectId(postId)));
      }
    }
  };

  // In the most basic sense, the ApolloServer can be started
  // by passing type definitions (typeDefs) and the resolvers
  // responsible for fetching the data for those types.
  const server = new ApolloServer({ typeDefs, resolvers });

  // This `listen` method launches a web-server.  Existing apps
  // can utilize middleware options, which we'll discuss later.
  server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
  });
});
