const { gql } = require('apollo-server');
//
// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.
  type Post {
    _id: ID
    title: String
    content: String
    comments: [Comment]
  }

  type Comment {
    _id: ID
    content: String
    post: Post
  }

  type Query {
    post(id: String): Post
    posts: [Post]
    comment(id: String): Comment
  }

  type Mutation {
    createPost(title: String, content: String): Post
    createComment(postId: String, content: String): Comment
  }
`;

module.exports = typeDefs;
